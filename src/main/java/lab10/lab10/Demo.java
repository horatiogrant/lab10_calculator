
package lab10.lab10;

import java.util.Date;
import java.util.Calendar;

public class Demo {
    
    public static void main(String[]args){
         Tool tool = new Tool();
          Date date = new Date();
           Calendar datejoined = Calendar.getInstance();
   
        Employee emp1 = new Employee("101","Bob",datejoined.getTime(),7000);
        
        System.out.println("Will emp get promotion? "+tool.isPromotionDueThisYear(emp1, true));
        System.out.println(tool.calculateTaxForCurrentYear(emp1));

    }
    
}
