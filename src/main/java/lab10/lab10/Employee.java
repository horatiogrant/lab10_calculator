
package lab10.lab10;

import java.util.Calendar;
import java.util.Date;


public class Employee {
    private String employeeID;
    private String name;
    private Date date;
    private double yearlyWage;

    public Employee(String employeeID, String name, Date date, double yearlyWage) {
        this.employeeID = employeeID;
        this.name = name;
        this.date = date;
        this.yearlyWage = yearlyWage;
    }
    
    private String getEmployeeID(){
        return employeeID;
    }
    public Date getEmployeeDateOfJoining(){
        return date;
    }
    
    public String getEmployeeName(){
        
        return name;
    }
    
    public double getWage(){
        return yearlyWage;
    }
}
